package com.example.laboratoriodos

interface ContadorListener {
    fun incrementar()
    fun getValorActual(): Int
    fun resetear()
    fun reducir()


}