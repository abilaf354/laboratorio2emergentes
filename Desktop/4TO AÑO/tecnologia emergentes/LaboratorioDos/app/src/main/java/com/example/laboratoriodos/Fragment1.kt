package com.example.laboratoriodos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class Fragment1 : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

   //aqui declaramos el objeto contadorListener
    var ContadorListener: ContadorListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    /* declaramos variale y adicionamos el contadorListener */
     var contadorb: ContadorListener?= null
    fun addfragmentoUnoContadorListener(c:ContadorListener){
        this.contadorb =c

    }

    /* declaramos variable y creamos la funcion onViewCreated y hacemos para incrementar */
    /*var contador = 0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val funcionTexto = view.findViewById<TextView>(R.id.txt1)
        funcionTexto.setText(contador.toString())
        val botonContar = view.findViewById<Button>(R.id.buttonContar)
        botonContar.setOnClickListener{
            funcionTexto.setText((contador++).toString())
        }
    }
      */
    /* declaramos variable y creamos la funcion onViewCreated y hacemos con contadorb */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val funcionTexto = view.findViewById<TextView>(R.id.txt1)
        funcionTexto.setText(contadorb!!.getValorActual().toString())
        val botonContar = view.findViewById<Button>(R.id.buttonContar)
        botonContar.setOnClickListener{
            contadorb!!.incrementar()
            funcionTexto.setText((contadorb!!.getValorActual()).toString())
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_1, container, false)
    }


    companion object {


        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}