package com.example.laboratoriodos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class Fragment2 : Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    //aqui declaramos el objeto contadorListener
    var contadorListenerfra2: ContadorListener? = null

     /* adicionamos el contador*/
    fun addfragmentoDosListener(c:ContadorListener){
       this.contadorListenerfra2 = c
    }

    /* declaramos variable y creamos la funcion onViewCreated y hacemos con contadorb */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val funcionTexto2 = view.findViewById<TextView>(R.id.txt2)
        val botonContar = view.findViewById<Button>(R.id.buttonReducir)
        botonContar.setOnClickListener{
            contadorListenerfra2?.reducir();
            funcionTexto2.setText((contadorListenerfra2!!.getValorActual()).toString())
        }
    }

    override fun onResume() {
        val text2 = view?.findViewById<TextView>(R.id.txt2)
        text2!!.setText(contadorListenerfra2?.getValorActual().toString())
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_2, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment2().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}