package com.example.laboratoriodos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FragmentResetear : Fragment() {
    var contadorListener: ContadorListener? = null
    private var param1: String? = null
    private var param2: String? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_resetear, container, false)

        val resetButton = view.findViewById<Button>(R.id.button2)
        resetButton.setOnClickListener {
            // Llama al método de reseteo del MainActivity
            (activity as? MainActivity)?.resetear()
            Toast.makeText(activity, "Valores reiniciados", Toast.LENGTH_SHORT).show()
        }

        return view
    }


}