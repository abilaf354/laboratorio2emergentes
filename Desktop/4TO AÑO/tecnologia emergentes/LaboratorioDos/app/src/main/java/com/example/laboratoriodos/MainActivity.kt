package com.example.laboratoriodos

// Importa los fragmentos FragmentUno y FragmentDos
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.laboratoriodos.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), ContadorListener {
    //----------creamos variables------------------
    var fra1: Fragment1? = null

    var fra2: Fragment2? = null
    var fra3: FragmentResetear? = null
    var buttonUno: Button? = null
    var buttonDos: Button? = null
    var buttonTres: Button? = null

    /*  llamamos los metodos del interface */
    var contarx = 0
    override fun incrementar() {
        contarx++
    }

    override fun getValorActual(): Int {
        return contarx
    }

    override fun resetear() {
        contarx = 0
    }


    override fun reducir() {
        if (contarx > 0)
            contarx--
        else
            contarx = 0
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //agregamos el toobar en el main
        binding  = ActivityMainBinding.inflate(layoutInflater)
       setContentView(binding.root)
       val toolbar = binding.idconectarToolbar.eslaIDtoolbar
        //metodo set
        setSupportActionBar(toolbar)


        // metodo de los fragmentos
        fra1 = Fragment1()
        fra2 = Fragment2()
        fra3 = FragmentResetear()
        /* enviar la implementacion */
        fra1?.addfragmentoUnoContadorListener(this)
        fra2?.addfragmentoDosListener(this)

        buttonUno = findViewById(R.id.buttonUno)
        buttonDos = findViewById(R.id.buttonDos)
        buttonTres = findViewById(R.id.buttonTres)


        buttonUno?.setOnClickListener {
            Toast.makeText(this, "Abriendo fragmento Uno", Toast.LENGTH_SHORT).show()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frameContenedor, fra1!!)
            transaction.commit()
        }

        buttonDos?.setOnClickListener {
            Toast.makeText(this, "Abriendo fragmento Dos", Toast.LENGTH_SHORT).show()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frameContenedor, fra2!!)
            transaction.commit()
        }

        buttonTres?.setOnClickListener {
            Toast.makeText(this, "Abriendo fragmento Tres", Toast.LENGTH_SHORT).show()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frameContenedor, fra3!!)
            transaction.commit()
        }

    }

    //metodo para añadir el actionBar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actionbar, menu)
        return true
    }

    //metodo para que aga accion los botones de guardar y ajustes
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itemGardar -> {
                Toast.makeText(this, "Guardar", Toast.LENGTH_SHORT).show()
                true
            }

            R.id.ItemAjustes -> {
                Toast.makeText(this, "Ajustes", Toast.LENGTH_SHORT).show()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

}